FROM php:7.4
WORKDIR /workdir
COPY . .
COPY xdebug/xdebug-local.ini /usr/local/etc/php/conf.d/xdebug-local.ini
RUN apt-get update && apt-get install --yes \
    unzip \
    procps

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN rm composer-setup.php

RUN curl --silent --location https://deb.nodesource.com/setup_14.x --output nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN rm nodesource_setup.sh
RUN apt install nodejs --yes

RUN pecl install xdebug
RUN make install