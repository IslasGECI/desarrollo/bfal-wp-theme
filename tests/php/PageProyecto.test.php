<?php

use PHPUnit\Framework\TestCase;
use Mockery;

function get_the_ID()
{
}
function get_the_title()
{
}
function wp_reset_postdata()
{
}
function get_the_content()
{
}
function apply_filters()
{
}
class PageProyectoTest extends TestCase
{
    public function testPageProyecto()
    {
        $WP_Query = Mockery::mock("overload:WP_Query");
        $WP_Query->shouldReceive('post_count');
        $WP_Query->shouldReceive('have_posts');
        $WP_Query->shouldReceive('the_post');
        PageProyecto::showTitlesSideBar();
        PageProyecto::renderExistingPosts();
        $expected = 1;
        $obtained = 1;
        $this->assertEquals($expected, $obtained);
        Mockery::close();
    }
}
