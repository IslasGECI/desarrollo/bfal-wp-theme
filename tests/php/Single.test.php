<?php

use PHPUnit\Framework\TestCase;

function get_the_author_meta()
{
    return "Científico de Datos";
}

function get_cupp_meta()
{
    return "url_photograph";
}

class SingleTest extends TestCase
{
    public function testPrintAuthorMetaWithEntry()
    {
        Single::printAuthorMetaWithEntry("single-card");
        $expected = 1;
        $obtained = 1;
        $this->assertEquals($expected, $obtained);
    }
    public function testGetPosition()
    {
        $obtained_position = Single::getPosition();
        $expected_position = "Científico de Datos";
        $this->assertEquals($expected_position, $obtained_position);
    }
    public function testGetUrlProfilePhoto()
    {
        $obtained_url = Single::getUrlProfilePhoto();
        $expected_url = "url_photograph";
        $this->assertEquals($expected_url, $obtained_url);
    }
}
