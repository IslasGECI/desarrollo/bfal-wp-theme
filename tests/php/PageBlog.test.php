<?php

use PHPUnit\Framework\TestCase;
use Mockery;

function get_permalink()
{
}

function get_the_date()
{
}

function wp_trim_words()
{
}


class PageBlogTest extends TestCase
{
    public function testPrintSlug()
    {
        $this->assertIsPublicMethon('PageBlog::printSlug');
    }

    public function testPrintDate()
    {
        $this->assertIsPublicMethon('PageBlog::printDate');
    }

    public function testGetPostList()
    {
        $WP_Query = Mockery::mock("overload:WP_Query");
        $WP_Query->shouldReceive('post_count');
        $WP_Query->shouldReceive('have_posts');
        $this->assertIsPublicMethon('PageBlog::getPostList');
    }

    public function testPrintTrimmedContent()
    {
        $this->assertIsPublicMethon('PageBlog::printTrimmedContent');
    }

    public function assertIsPublicMethon($method)
    {
        $method();
        $expected = 1;
        $obtained = 1;
        $this->assertEquals($expected, $obtained);
    }
}
