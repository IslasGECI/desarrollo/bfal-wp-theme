const sum = require("../../libraries/js/add");

describe("test add", () => {
    test("adds 1 + 2 to equal 3", () => {
        expect(sum(1, 2)).toBe(3);
    });
});
