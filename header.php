<?php
/**
 * The header for the theme
 *
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>
    >

    <head>
        <link
            rel="shortcut icon"
            type="image/svg"
            href="<?php Header::printBlogUrl(); ?>/assets/img/albatros_boton.svg"
        />
        <meta charset="
        <?php bloginfo("charset"); ?>
        ">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php wp_head(); ?>
    </head>

    <body>
        <div id="wrapper">
            <header>
                <nav
                    class="navbar fixed-top navbar-expand-lg navbar-light bg-white d-block pb-0 mb-3"
                >
                    <div class="container">
                        <a class="navbar-brand" href="https://www.islas.org.mx" target="_blank">
                            <img
                                src="<?php Header::printBlogUrl(); ?>/assets/img/logo.svg"
                                height="63"
                            />
                        </a>
                        <button
                            class="navbar-toggler collapsed"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarId"
                            aria-controls="navbarsExample07"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                        >
                            <span class="navbar-toggler-icon">
                                <img
                                    src="<?php Header::printBlogUrl(); ?>/assets/img/menu_sandwich.svg"
                                />
                            </span>
                        </button>

                        <div class="navbar-collapse collapse" id="navbarId">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="hide-on-small">
                                    <a
                                        id="albatros-boton"
                                        class="mx-2"
                                        href="/albatros-patas-negras/"
                                        data-bs-toggle="tooltip"
                                        data-bs-placement="top"
                                        title="Inicio"
                                        ><img
                                            src="<?php Header::printBlogUrl(); ?>/assets/img/albatros_boton.svg"
                                            width="50"
                                        />&nbsp;&nbsp;&nbsp;&nbsp;</a
                                    >
                                </li>
                                <li class="nav-item hide-on-large">
                                    <a class="nav-link active" href="/albatros-patas-negras/"
                                        >Inicio</a
                                    >
                                </li>
                                <li class="nav-item">
                                    <a
                                        class="nav-link active"
                                        href="/albatros-patas-negras/proyecto"
                                        >Proyecto</a
                                    >
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="/albatros-patas-negras/blog"
                                        >Blog</a
                                    >
                                </li>
                            </ul>
                        </div>
                    </div>
                    <section id="inner-headline">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="hidden-sm col-md-2"></div>
                                    <div class="col-md-10"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </nav>
            </header>
        </div>
    </body>
</html>
