<?php
/**
 * The template for displaying all single posts
 *
 */

get_header();
?>

<section id="hacemos_content" class="cushion-xs conservacion1">
    <div class="container mb-2 mt-4 ml-3">
        <?php while (have_posts()): the_post(); ?>
        <article class="section">
            <div class="row">
                <div class="col-12">
                    <p class="card-date"><?php PageBlog::printDate(); ?></p>
                    <h1><?php the_title(); ?></h1>
                    <div class="d-inline-block mt-3 mb-5"> 
                        <?php Single::printAuthorMetaWithEntry("single-card"); ?> 
                    </div>
                    <?php echo get_the_content(); ?>
                </div>
            </div>
        </article>
        <?php endwhile;?>
    </div>
</section>

<?php
get_footer();
?>
