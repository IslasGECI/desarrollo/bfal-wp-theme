<?php
/**
 * The template for displaying the footer
 *
 */
?>

<footer class="desktop-footer">
    <section class="container">
        <div class="details">
            <a class="navbar-brand" href="#">
                <img src="<?php echo get_bloginfo(
                    "template_url"
                ); ?>/assets/img/logo_footer.svg">
            </a>
            <div class="contacto">
                contacto@islas.org.mx
                <br />
                Avenida Moctezuma #836, Zona Centro, C.P. 22800
                <br />
                Ensenada, Baja California
            </div>

            <div class="icons">
                <a href="https://www.facebook.com/IslasGECI/" target="_blank"
                    ><img src="<?php echo get_bloginfo(
                    "template_url"
                ); ?>/assets/img/facebook.svg"></a
                >
                <a href="https://www.instagram.com/islasgeci/" target="_blank"
                    ><img src="<?php echo get_bloginfo(
                    "template_url"
                ); ?>/assets/img/instagram.svg"></a
                >
                <a href="https://www.youtube.com/channel/UChCwUNW27D50Bwh27U0lpfg" target="_blank"
                    ><img src="<?php echo get_bloginfo(
                    "template_url"
                ); ?>/assets/img/youtube.svg"></a
                >
                <a href="https://twitter.com/islasgeci" target="_blank"
                    ><img src="<?php echo get_bloginfo(
                    "template_url"
                ); ?>/assets/img/Twitter.svg"></a
                >
                <a href="https://github.com/IslasGECI" target="_blank"
                    ><img src="<?php echo get_bloginfo(
                    "template_url"
                ); ?>/assets/img/github.svg"></a
                >
            </div>
        </div>
    </section>
</footer>
<footer class="mobile-footer">
    <section>
        <img
            class="logo"
            src="https://www.islas.org.mx/albatros-patas-negras/wp-content/themes/bfal-wp-theme/assets/img/logo_footer.svg"
            width="70"
        />
        <div class="details">
            <div class="icons">
                <a href="https://www.facebook.com/IslasGECI/" target="_blank"
                    ><img
                        src="https://www.islas.org.mx/albatros-patas-negras/wp-content/themes/bfal-wp-theme/assets/img/facebook.svg"
                /></a>
                <a href="https://www.instagram.com/islasgeci/" target="_blank"
                    ><img
                        src="https://www.islas.org.mx/albatros-patas-negras/wp-content/themes/bfal-wp-theme/assets/img/instagram.svg"
                /></a>
                <a href="https://www.youtube.com/channel/UChCwUNW27D50Bwh27U0lpfg" target="_blank">
                    <img
                        src="https://www.islas.org.mx/albatros-patas-negras/wp-content/themes/bfal-wp-theme/assets/img/youtube.svg"
                    />
                </a>
                <a href="https://twitter.com/islasgeci" target="_blank"
                    ><img
                        src="https://www.islas.org.mx/albatros-patas-negras/wp-content/themes/bfal-wp-theme/assets/img/Twitter.svg"
                /></a>
                <a href="https://github.com/IslasGECI" target="_blank"
                    ><img
                        src="https://www.islas.org.mx/albatros-patas-negras/wp-content/themes/bfal-wp-theme/assets/img/github.svg"
                /></a>
            </div>
            <div class="contacto">contacto@islas.org.mx</div>
        </div>
    </section>
</footer>

<?php wp_footer(); ?>
