<?php
/**
 * The template for displaying all pages
 *
 */

get_header(); ?>
<section id="hacemos_content" class="cushion-xs conservacion1">
    <div class="container mt-4 ml-3 row1">
        <div class="column1">
            <div class="col-2 hide">
                <div style="position: fixed">
                    <?php
                        PageProyecto::showTitlesSideBar();
                    ?>
                </div>
            </div>
        </div>
        <div class="column2">
            <?php
                PageProyecto::renderExistingPosts();
            ?>
        </div>
    </div>
    <a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button">
        <i class="fa fa-arrow-up"></i>
    </a>
</section>

<?php get_footer();?>
