# [Tema oficial para el Blog de Albatros Patas Negras](https://www.islas.org.mx/albatros-patas-negras)

## Caracteristicas

* [Wordpress theme files](https://developer.wordpress.org/themes/basics/template-files/)
* CSS

## Cómo correr las pruebas

Para correr las pruebas en tu estación de trabajo:

```shell
git clone https://gitlab.com/IslasGECI/desarrollo/bfal-wp-theme.git
cd bfal-wp-theme
git checkout <RAMA-A-PROBAR> # Reemplaza <RAMA-A-PROBAR> con el nombre de la rama correspondiente
docker build --tag islasgeci/bfal-wp-theme .
docker run islasgeci/bfal-wp-theme make tests
```

Si prefieres correr las pruebas adentro del contenedor solo ejecuta `make tests` adentro del
contenedor.
