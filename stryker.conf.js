module.exports = {
    mutate: ["./libraries/js/*.js"],
    packageManager: "npm",
    reporters: ["html", "clear-text", "progress"],
    jest: {
        projectType: "custom",
        enableFindRelatedTests: true,
    },
    coverageAnalysis: "off",
    commandRunner: {
        command: 'npm run tests'
    }
}
