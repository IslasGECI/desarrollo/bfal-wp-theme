<?php

/**
 * The template for displaying all pages
 *
 */

get_header(); ?>
<div id="content" class="site-content mt-4 page-blog">
    <main id="main" class="site-main" role="main">
        <div class="container mb-2">
            <div class="grid-container1 mb-5">
                <?php
                $i = 0;
                $list = ["A", "B", "C"];
                $query = PageBlog::getPostList(); ?>
                <?php while ($query->have_posts() && $i < 3) : $query->the_post(); ?>
                <div class="card border-0 <?php echo $list[$i]; ?>">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <a href="<?php PageBlog::printSlug(); ?>">
                                <img
                                    class="card-img-top img-fluid rounded"
                                    src="<?php the_post_thumbnail_url() ?>"
                                />
                            </a>
                        </div>
                        <div class="col-md-8 justify-column">
                            <div class="card-body">
                                <p class="card-date"><?php PageBlog::printDate(); ?></p>
                                <a href="<?php PageBlog::printSlug(); ?>">
                                    <h5 class="card-title titulo">
                                        <?php the_title(); ?>
                                    </h5>
                                </a>
                                <p class="card-text mb-5">
                                    <?php PageBlog::printTrimmedContent(); ?>
                                </p>
                                <?php Single::printAuthorMetaWithEntry("right-side"); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++;
                endwhile; ?>
            </div>
            <div class="grid-container2 mb-5">
                <?php
                $j = 0;
                $query = PageBlog::getPostList(); ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                <?php if (
                        $j >
                2 ) { ?>
                <div class="card border-0 Z">
                    <div class="row g-0">
                        <div class="col-md-4 three-images-row">
                            <a href="<?php PageBlog::printSlug(); ?>">
                                <img
                                    class="card-img-top img-fluid rounded"
                                    src="<?php the_post_thumbnail_url() ?>"
                                />
                            </a>
                        </div>
                        <div class="col-md-8 justify-column">
                            <div class="card-body">
                                <p class="card-date"><?php PageBlog::printDate(); ?></p>
                                <a href="<?php PageBlog::printSlug(); ?>">
                                    <h5 class="card-title titulo">
                                        <?php the_title(); ?>
                                    </h5>
                                </a>
                                <p class="card-text mb-5">
                                    <?php PageBlog::printTrimmedContent(); ?>
                                </p>
                            </div>
                            <?php Single::printAuthorMetaWithEntry("right-side"); ?>
                        </div>
                    </div>
                </div>
                <?php } else $j++; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </main>
</div>
<?php
get_footer();
?>
