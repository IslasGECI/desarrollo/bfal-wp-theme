.PHONY: \
		check \
		check_css \
		check_html
		check_js \
		check_php \
		clean \
		format \
		format_css \
		format_html \
		format_js \
		format_php \
		install \
		mutants \
		mutants_js \
		mutants_php \
		tests \
		tests_js \
		tests_php 

check: install check_css check_html check_js check_php

check_css:
	npm run prettier:check_css

check_html:
	npm run prettier:check_root_files_html

check_js:
	npm run prettier:check_libraries_and_tests_js

check_php:
	composer phpcs:check_functions_php
	composer phpcs:check_libraries_and_tests_php

clean:
	rm --force --recursive .stryker-tmp
	rm --force --recursive node_modules
	rm --force --recursive reports
	rm --force --recursive vendor
	rm --force .phpunit.result.cache
	rm --force composer.lock
	rm --force package-lock.json

format: install format_css format_php format_js format_html

format_css:
	npm run prettier:format_css

format_html:
	npm run prettier:format_root_files_html

format_js:
	npm run prettier:format_libraries_and_tests_js

format_php:
	composer phpcbf:format_libraries_and_tests_php
	composer phpcbf:format_functions_php

install:
	composer install
	npm install

mutants: install mutants_php mutants_js

mutants_js:
	npm run mutants | tee /dev/tty | grep "Survived" && exit 1 || exit 0

mutants_php:
	export XDEBUG_MODE=coverage && ./vendor/bin/infection --show-mutations | tee /dev/tty | grep "Mutation Score Indicator (MSI): 100%" && exit 0 || exit 1

tests: install tests_js tests_php

tests_js:
	npm run tests

tests_php:
	./vendor/bin/phpunit --verbose
