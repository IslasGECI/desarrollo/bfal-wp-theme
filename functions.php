<?php

/**
 * Functions and definitions
 *
 */
include('libraries/php/blog-class.php');
include('libraries/php/header-class.php');
include('libraries/php/project-class.php');
include('libraries/php/single-class.php');

/**
 * Enqueue scripts and styles.
 */
function bfal_scripts()
{
    wp_enqueue_style("bfal-style", get_stylesheet_uri());
    wp_enqueue_style(
        "general-style",
        get_template_directory_uri() . "/assets/css/custom/general.css"
    );
    wp_enqueue_style(
        "landing-page-style",
        get_template_directory_uri() . "/assets/css/custom/index.css"
    );
    wp_enqueue_style(
        "page-blog-style",
        get_template_directory_uri() . "/assets/css/custom/page-blog.css"
    );
    wp_enqueue_style(
        "page-proyecto-style",
        get_template_directory_uri() . "/assets/css/custom/page-proyecto.css"
    );
    wp_enqueue_style(
        "single-blog-entry",
        get_template_directory_uri() . "/assets/css/custom/single.css"
    );
    wp_enqueue_style(
        "footer-style",
        get_template_directory_uri() . "/assets/css/custom/footer.css"
    );
    wp_enqueue_style(
        "bootstrap-style",
        get_template_directory_uri() . "/assets/css/bootstrap5.min.css"
    );
    wp_enqueue_style(
        "bootstrap-style",
        get_template_directory_uri() . "/assets/css/bootstrap5.min.css.map"
    );

    wp_enqueue_style(
        "font-awesome-style",
        get_template_directory_uri() . "/assets/css/font-awesome.css"
    );

    wp_enqueue_script(
        "bootstrap-scripts",
        get_template_directory_uri() . "/assets/js/bootstrap5.min.js"
    );
    wp_enqueue_script(
        "bootstrap-scripts",
        get_template_directory_uri() . "/assets/js/bootstrap5.min.js.map"
    );
}

add_action("wp_enqueue_scripts", "bfal_scripts");
add_theme_support("post-thumbnails");
