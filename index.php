<?php

/**
 * The main template file
 */

get_header(); ?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="desktop-cover" src="<?php echo get_bloginfo("template_url"); ?>/assets/img/landing-page-1.png">
            <img class="mobile-cover" src="<?php echo get_bloginfo("template_url"); ?>/assets/img/landing-page-movil.png">
        </div>
    </div>
</div>
<div id="content" class="site-content mt-5">
    <main id="main" class="site-main" role="main">
        <div class="container mb-2">
            <div class="grid-container2 mb-5">
                <?php
                    $j = 0;
                    $query = new WP_Query(["category_name" =>
                "blog","order" => "DESC"]); ?>
                <?php while ($query->have_posts() && $j <= 2): $query->the_post();?>
                <div class="card border-0 Z">
                    <div class="row g-0">
                        <div class="col-md-4 three-images-row">
                            <a href="<?php echo get_permalink();?>">
                                <img
                                    class="card-img-top img-fluid rounded"
                                    src="<?php the_post_thumbnail_url() ?>"
                                />
                            </a>
                        </div>
                        <div class="col-md-8 justify-column">
                            <div class="card-body">
                                <p class="card-date"><?php PageBlog::printDate(); ?></p>
                                <a href="<?php echo get_permalink();?>">
                                    <h5 class="card-title titulo">
                                        <?php the_title(); ?>
                                    </h5>
                                </a>
                                <p class="card-text mb-5">
                                    <?php PageBlog::printTrimmedContent(); ?>
                                </p>
                            </div>
                            <?php Single::printAuthorMetaWithEntry("right-side"); ?>
                        </div>
                    </div>
                </div>
                <?php $j++; endwhile; ?>
            </div>
        </div>
    </main>
</div>
<?php
get_footer();?>
