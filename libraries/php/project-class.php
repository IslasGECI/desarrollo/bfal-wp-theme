<?php

class PageProyecto
{
    private static $args = [
        "category_name" => "proyecto",
        "order" => "ASC",
    ];

    public static function showTitlesSideBar()
    {
        $query = new WP_Query(self::$args);
        echo $query->post_count();

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $the_post = $query->the_post();
                $the_id = get_the_ID();
                echo '<a href="#post-' . $the_id . '" class="title-side-menu">';
                echo get_the_title();
                echo "</a><br>";
            }
        }
    }

    public static function renderExistingPosts()
    {
        $query = new WP_Query(self::$args);

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $the_post = $query->the_post();
                $the_id = get_the_ID();
                echo '<article id="post-' . $the_id . '" class="section">';
                echo '<div class="row"><div class="col-12 texto">';
                $content = get_the_content();
                $content = apply_filters("the_content", $content);
                echo $content;
                echo "</div></div></article>";
            }
        }
    }
}
