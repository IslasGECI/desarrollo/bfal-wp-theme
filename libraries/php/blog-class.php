<?php

class PageBlog
{
    public static function printSlug()
    {
        $slug = basename(get_permalink());
        echo $slug;
    }
    public static function printDate()
    {
        echo get_the_date('j \d\e F, Y');
    }
    public static function printTrimmedContent()
    {
        echo wp_trim_words(get_the_content(), 20, '...');
    }
    public static function getPostList()
    {
        $args = ["category_name" => "blog","order" => "DESC"];
        return new WP_Query($args);
    }
}
