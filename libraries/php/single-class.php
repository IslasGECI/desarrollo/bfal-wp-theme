<?php

class Single
{
    public static function printAuthorMetaWithEntry($class_name)
    {
        $author_name = get_the_author_meta('display_name');
        $author_position = self::getPosition();

        $user_id = get_the_author_meta('ID');
        $size = 'thumbnail';
        $url_profile_photo = self::getUrlProfilePhoto();
        
        echo '<div id="' .$class_name . '">';
        echo '    <div id="profile-pic">';
        echo '        <img src="' . $url_profile_photo . '" alt="" id="profilepic" />';
        echo '    </div>';
        echo '    <div class="author-name" id="profile-name">';
        echo           $author_name;
        echo '    </div>';
        echo '    <div class="author-role" id="rol">';
        echo            $author_position;
        echo '    </div>';
        echo '</div>';
    }

    public static function getPosition()
    {
        $author_position = get_the_author_meta('description');
        if (!$author_position) {
            $author_position = 'Especialista en Conservación';
        }
        return $author_position;
    }

    public static function getUrlProfilePhoto()
    {
        $url_profile_photo = get_cupp_meta($user_id, $size);
        if (!$url_profile_photo) {
            $url_bfal = 'https://www.islas.org.mx/albatros-patas-negras/';
            $url_photo = 'wp-content/uploads/2021/06/default-150x150.jpeg';
            $url_profile_photo = $url_bfal . $url_photo;
        }
        return $url_profile_photo;
    }
}
